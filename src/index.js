const fs = require("fs");
const {recursivelyProcessHelmDirectory, writeHelmDataToCSV} = require("./helm-parser");
const {processVMWareExports, writeVMDataToCSV} = require("./vm");
const path = require("path");

function createOutputDirectory(outputDir){
    if(!fs.existsSync(outputDir)){
        fs.mkdirSync(outputDir);
    }
}

const mode = process.env.AWSMA_MODE || "VM"
const targetDirectory = process.env.AWSMA_SOURCE_DIR || path.join(process.cwd(), "data", "vm");
const outputDirectory = process.env.AWSMA_OUTPUT_DIR || path.join(process.cwd(), "reports", "vm")

if(!fs.existsSync(targetDirectory)){
    console.log("The target directory does not exist");
    process.exit(2);
}

const stats = fs.statSync(targetDirectory);

if(!stats.isDirectory()) {
    console.log("The specified target is not a directory");
    process.exit(3);
}

createOutputDirectory(outputDirectory);

try {

    switch(mode){
        case "VM":
            const {machines} = processVMWareExports(targetDirectory);
            writeVMDataToCSV(outputDirectory, machines);
            break;

        case "HELM":
        default:
            const apps = recursivelyProcessHelmDirectory(targetDirectory);
            writeHelmDataToCSV(outputDirectory, apps);
            break;
    }

} catch(e){
    console.log("An unknown error has occurred.");
    console.error(e);
    process.exit(-1);
}

