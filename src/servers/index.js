const {splitEvery, join, slice, last, equals, toLower} = require("ramda");
const config = {
    vmNameLookups: {
        legacy: {
            tiers: {
                d: {
                    name: "dev",
                    environments: {
                        "0": "sandbox",
                        "1": "cie",
                    }
                },
                q: {
                    name: "qa",
                    environments: {
                        "3": "qa3",
                        "1": "qa1",
                    }
                },
                p: {
                    name: "prod",
                    environments: {
                        "0": "via-west",
                        "1": "aws-production"
                    }
                },
            },
            deviceRoles: {
                ans: "Ansible Server",
                api: "api server",
                bat: "batch processing box for db cron jobs, etc",
                cap: "capsol server",
                cch: "coherence server",
                cfs: "betti file upload server",
                com: "comms server",
                con: "consumer site server",
                crm: "betti server",
                fil: "file server/cmt",
                kaf: "kafka server",
                log: "rsyslog Server",
                msv: "microservice server",
                pro: "sp site server",
                sch: "ElasticSearch server",
                sal: "Avaya Gateway Server",
                utl: "utility server for watcher/background/phonequeue/etc web webserver",
                zoo: "zookeeper server",
            }
        },
        modern: {
            tiers: {
                p: "prod",
                q: "qa",
                d: "dev",
                e: "enterprise",
                t: "test"
            },
            teams: {
                i: "infosec",
                n: "networking",
                s: "storage",
                t: "telco",
                u: "unix",
                w: "windows"
            },
            siteCodes: {
                "00": {
                    name: "vw-cornell",
                    environments: {
                        "0": "production-cage-27",
                        "1": "enterprise-cage-18",
                        "3": "enterprise-cage-18-dmz",
                    }
                },
                "01": {
                    name: "aws",
                    environments: {},
                },
                "02": {
                    name: "vw-compark",
                    environments: {
                        "0": "production",
                        "1": "enterprise",
                    }
                },
                "03": {
                    name: "newyork",
                    environments: {
                        "0": "primary-site",
                        "1": "2-penn",
                        "3": "ny-office-iac-building"
                    }
                },
                "04": {
                    name: "indianapolis",
                    environments: {
                        "0": "virginia",
                        "1": "mass-ave",
                        "2": "amco",
                        "3": "130-washington"
                    }
                },
                "05": {
                    name: "kansas city",
                    environments: {
                        "0": "lenexa",
                        "2": "olathe",
                    }
                },
                "06": {
                    name: "denver",
                    environments: {
                        "0": "lodo-1",
                        "1": "lodo-2",
                        "3": "hub",
                    }
                },
                "07": {
                    name: "golden",
                    environments: {
                        "0": "north",
                        "1": "west",
                        "2": "south",
                    }
                },
                "08": {
                    name: "colorado-springs",
                    environments: {
                        "0": "primary-site",
                    }
                },
                "09": {
                    name: "azure",
                    environments: {
                        "1": "wcu",
                        "2": "ncu",
                    }
                },
                "10": {
                    name: "lightbound",
                    environments: {
                        "0": "production"
                    }
                },
                "11": {
                    name: "chicago",
                    environments: {

                    }
                },
            },
            deviceRoles: {
                aas: "Avaya Aura Device Services",
                ads: "Active Directory",
                adf: "Active Directory Federation Services",
                aes: "Avaya AES Server",
                acs: "Access Switch",
                afs: "Atlassian File Server",
                ags: "Avaya Gateway Server",
                acm: "Avaya Communication Manager",
                aep: "Avaya Experience Portal",
                ars: "Avaya RSI Reporting",
                asm: "Avaya System Manager",
                ans: "Ansible Server",
                api: "API Server",
                ast: "Asterisk Server",
                att: "Attunity Server",
                atx: "Atex Server",
                aud: "Auditing Server",
                aut: "Automate BPA Server",
                bat: "Batch processing box for db cron jobs, etc",
                bnc: "sendmail bounce server (a la pbounce001)",
                can: "Canary (SYS-1382)",
                cap: "Capsol",
                cbt: "CertBot (LetsEncrypt)",
                ccc: "Avaya Control Manager",
                ccm: "SCCM Server",
                ces: "Comm Engine Server",
                cfl: "CloudFlare Logs Host",
                cfs: "CRM File Server",
                chw: "Cherwell",
                cmc: "Dell Chassis Management Console",
                cmr: "CallMiner",
                cnf: "Atlasian Confluence",
                coh: "Coherence Server",
                com: "Comms Server",
                con: "Consumer Site Server",
                cpm: "ClearPass Policy Manager",
                crm: "Betti/CRM Server",
                crs: "Core Switch",
                crt: "Certificate based services",
                dcs: "Digicert Cert Scanner",
                dms: "Demisto Server (infosec)",
                dns: "Bind DNS Server",
                doc: "Docker Server",
                dhp: "DHCP Server",
                drc: "Dell Remote Access Console",
                dss: "Distribution Switch",
                dsb: "Devops Sandbox",
                dvt: "Dev Tools Server (Zamboni, Etc..)",
                esh: "ElasticSearch Server",
                esx: "ESXi Server",
                exa: "Exagrid Server",
                fil: "Files Server - for website files/attachments",
                fwl: "Firewall",
                frm: "Foreman (POC)",
                gfs: "Gluster Cluster Server",
                grf: "Grafana Server",
                git: "Git Server",
                hci: "HCI - Hyper-converged infrastructure boxes - Netapp",
                hva: "handy vena vms",
                icg: "IGEL Cloud Gateway proxy",
                jls: "FreeBSD Jail Server",
                jra: "Atlassian Jira",
                inf: "Informatica Enterprise",
                ipa: "free IPA",
                jen: "Jenkins Master Node",
                jmf: "Jamf server (Winadmin)",
                jmp: "JumpBox Server",
                jwn: "Jenkins Worker Node",
                kaf: "Kafka Server",
                kbm: "Kubernetes Master",
                kbw: "Kubernetes Worker",
                kmw: "Kubernetes Monitoring Worker (Prometheus)",
                kfm: "Kafka Mirrormaker",
                ktm: "Kubernetes Test Master Node",
                ktw: "Kubernetes Test Worker Node",
                lbr: "Loadbalancer",
                lmr: "Lemur Server",
                log: "Syslog Server",
                msp: "Membrane Service Proxy",
                msq: "MySQL Server",
                msv: "microservice Server",
                nbx: "Netbox",
                nag: "Nagios",
                nec: "Nectar - Telephony VOIP Monitor",
                nap: "Netapp Device",
                nfs: "NFS Filesystem",
                oum: "OnCommand Unified Manager - for Netapp FAS/AFF",
                oms: "OpenManage System (OMIVV/OME)",
                ora: "Oracle DB Server",
                pgh: "Pigeon Hole Server",
                phq: "Phone Queue Server",
                phn: "Telco Phone server",
                ppt: "ProofPoint (TRAP)",
                pro: "SP Site Server",
                prx: "Proxy Server (haproxy, ngnix, squid)",
                ptm: "Prognosis telephony monitoring",
                rdc: "Oracle remote data center connector",
                rds: "Remote Desktop Server OS's (Citrix & VM Horizon too)",
                rp7: "Rapid 7 - Infosec security appliance scan tool",
                rsi: "Telephony RSI Shadow server",
                rst: "R-Studio",
                sae: "Dell SupportAssist Enterrpise Server",
                sal: "Avaya Secure Access Link",
                sat: "RHEL Satellite",
                scp: "SCP/FTP/SFTP/TFTP server which mounts proper storage for when there is no other option.",
                sec: "Security Server - Misc. Apps",
                sel: "Chrome Selenium",
                sfm: "SolidFire Management (SolidFire Element)",
                snd: "Developer Sandbox",
                sol: "solarwinds",
                spc: "SpaceWalk Server",
                str: "Stratodesk No Touch management console",
                svm: "Storage Virtual Machine (use netapp host numbers and append next digit ps000nap901 → ps000svm901-01",
                sql: "Microsoft SQL Server",
                tfm: "Terraform",
                tms: "Time Matters Server",
                tri: "Tritis",
                tso: "AWS TSO Migration Server",
                utl: "Utility Server for Watchers, Background, Phone Queues, etc",
                vcr: "vCenter",
                vcs: "Veritas Cluster Services (Oracle clustering)",
                vea: "Veeam Server",
                vdi: "Virtual Desktops (Win7, Win10 OS's)",
                vom: "VMware vRealize Operations Manager",
                vpm: "Voipmonitor - telco monitoring server",
                wap: "Web Application Proxy",
                wat: "Watcher Server",
                web: "Web Server",
                wfo: "Workforce Optimization (Telephony)",
                wlm: "Avaya web lm Server",
                win: "Generic Windows Server",
                wfs: "Windows File Server",
                zoo: "Zookeeper Server",
            }
        },
    }
};

const LEGACY_SERVER_NAME = Symbol("legacy-server-name");
const MODERN_SERVER_NAME = Symbol("modern-server-name");
const UNKNOWN_SERVER_NAME_TYPE = Symbol("unknown-server-name-type");



function getServerNameType(serverName) {
    const LEGACY_SERVER_NAME_REGEX = /^[p,q, d][0,1,3][a-zA-Z]{3}[0-9]*/gm
    const MODERN_SERVER_NAME_REGEX = /^[p,q,d,e,t][i,n,s,t,u,w][0-9]{2}[0-9][a-zA-Z]{3}[0-9]*/gm
    if (MODERN_SERVER_NAME_REGEX.test(serverName)) {
        return MODERN_SERVER_NAME;
    } else if (LEGACY_SERVER_NAME_REGEX.test(serverName)) {
        return LEGACY_SERVER_NAME;
    }
    return UNKNOWN_SERVER_NAME_TYPE;
}

function parseLegacyServerName(serverName) {
    const nameChars = splitEvery(1, serverName);
    const nameData = {
        tier: nameChars[0],
        environment: nameChars[1],
        deviceRole: join("", slice(2, 5, nameChars)),
    }

    return {
        tier: config.vmNameLookups.legacy.tiers[nameData.tier].name,
        environment: config.vmNameLookups.legacy.tiers[nameData.tier].environments[nameData.environment],
        role: config.vmNameLookups.legacy.deviceRoles[nameData.deviceRole] || nameData.deviceRole,
    }
}

function parseModernServerName(serverName) {
    const nameChars = splitEvery(1, serverName);
    const nameData = {
        tier: nameChars[0],
        siteCode: join("", slice(3, 5, nameChars)),
        environment: nameChars[4],
        deviceRole: join("", slice(5, 8, nameChars)),
        applicationTier: join("", slice(8, 12, nameChars)),
    }

    return {
        tier: config.vmNameLookups.modern.tiers[nameData.tier],
        environment: config.vmNameLookups.modern.siteCodes[nameData.siteCode] ? `${config.vmNameLookups.modern.siteCodes[nameData.siteCode].name}-${config.vmNameLookups.modern.siteCodes[nameData.siteCode].environments[nameData.environment]}` : nameData.siteCode,
        role: config.vmNameLookups.modern.deviceRoles[nameData.deviceRole] || nameData.deviceRole,
    }
}

function parseServerName(serverName){
    const workingServerName = toLower(serverName);
    const workingServerNameType = getServerNameType(workingServerName);
    if(equals(workingServerNameType, UNKNOWN_SERVER_NAME_TYPE)) return UNKNOWN_SERVER_NAME_TYPE;
    if(equals(workingServerNameType, MODERN_SERVER_NAME)) return parseModernServerName(workingServerName);
    if(equals(workingServerNameType, LEGACY_SERVER_NAME)) return parseLegacyServerName(workingServerName);
    throw new Error("error parsing server name");
}

module.exports = { parseServerName, parseLegacyServerName, parseModernServerName, getServerNameType, LEGACY_SERVER_NAME, MODERN_SERVER_NAME, UNKNOWN_SERVER_NAME_TYPE}