const wish = require("wish");
const {equals, join} = require("ramda");
const {parseLegacyServerName, parseModernServerName, getServerNameType, LEGACY_SERVER_NAME, UNKNOWN_SERVER_NAME_TYPE,
    MODERN_SERVER_NAME, parseServerName
} = require("./index");

describe("Server name parsing tests", () => {
    it("should return a legacy server name type", () => {
        const subjectUnderTest = "d0api001";
        const expected = LEGACY_SERVER_NAME;
        const actual = getServerNameType(subjectUnderTest);
        wish(equals(expected, actual));
    });
    it("should return a modern server name type", () => {
        const subjectUnderTest = "du000api006";
        const expected = MODERN_SERVER_NAME;
        const actual = getServerNameType(subjectUnderTest);
        wish(equals(expected, actual));
    });
    it("should return an unknown server name type", () => {
        const subjectUnderTest = "foo";
        const expected = UNKNOWN_SERVER_NAME_TYPE;
        const actual = getServerNameType(subjectUnderTest);
        wish(equals(expected, actual));
    });
    it("should parse a legacy server name", () => {
        const subjectUnderTest = "d0api001";
        const expected = {
            tier: "dev",
            environment: "sandbox",
            role: "api server"

        };
        const actual = parseLegacyServerName(subjectUnderTest);
        wish(equals(expected, actual));
    });
    it("should parse a modern server name", () => {
        const subjectUnderTest = "du000api006";
        const expected = {
            tier: "dev",
            environment: "vw-cornell-production-cage-27",
            role: "API Server"

        };
        const actual = parseModernServerName(subjectUnderTest);
        wish(equals(expected, actual));
    });
    it("should not parse an unknown server name", () => {
        const subjectUnderTest = "foo";
        const expected = UNKNOWN_SERVER_NAME_TYPE;
        const actual = parseServerName(subjectUnderTest);
        wish(equals(expected, actual));
    });
});