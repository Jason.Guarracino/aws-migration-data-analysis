const fs = require("fs");
const {parse} = require("csv-parse/sync");
const {stringify} = require("csv-stringify/sync");

const syspath = require("path");
const {
    map, filter, reduce, split, last, forEach, append, indexOf, includes, mergeLeft, insert, clone, equals, isEmpty,
    isNil, has
} = require("ramda");
const {parseServerName, UNKNOWN_SERVER_NAME_TYPE} = require("../servers");


function processVMWareExports(directoryPath) {
    const directoryContents = fs.readdirSync(directoryPath);
    const isFile = (itemPath) => {
        const stats = fs.statSync(syspath.join(directoryPath, itemPath));
        return stats.isFile();
    };
    const files = map(subDirectory => syspath.join(directoryPath, subDirectory), filter(isFile, directoryContents));
    return reduce((accum, value) => {
        const fileDataBuffer = fs.readFileSync(value);
        const parsed = parse(fileDataBuffer, {columns: true});
        const hasName = () => has("Name", parsed[0]);
        const hasState = () => has("State", parsed[0]);
        const hasIPAddress = () => has("IP Address", parsed[0]);
        const hasDNSName = () => has("DNS Name", parsed[0]);
        const hasCluster = () => has("Cluster", parsed[0]);

        if (!(!isNil(parsed) && !isEmpty(parsed) && hasDNSName() && hasState() && hasIPAddress() && hasName() && hasCluster())) {
            const updatedErrors = append({error: "missing fields", filePath: value}, accum.errors);
            return mergeLeft(accum, {errors: updatedErrors});
        }

        let updatedMachines = clone(accum.machines);

        forEach(row => {

            const nameData = parseServerName(row["Name"]);
            const description = equals(nameData, UNKNOWN_SERVER_NAME_TYPE) ? "None" : `${nameData.tier} - ${nameData.environment} - ${nameData.role}`;
            const machine = mergeLeft(row, {Description: description});
            updatedMachines = append(machine, updatedMachines);
        }, parsed);

        const updatedResult = mergeLeft({machines: updatedMachines}, accum);
        return updatedResult;
    }, {machines: [], errors: []}, files);
}

function writeVMDataToCSV(outputDir, machines) {
    const file = stringify(machines, {header: true});
    fs.writeFileSync(syspath.join(outputDir,"vm-migration-list-generated.csv"), Buffer.from(file));
}

module.exports = {
    processVMWareExports,
    writeVMDataToCSV,
    curried: {

    }
}