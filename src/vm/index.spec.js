const wish = require("wish");
const {equals} = require("ramda");
const {processVMWareExports} = require("./index");

describe("VMWare file parsing tests", () => {
   it("should parse valid VMWare files", () => {
      const expectedCount = 22;
      const actualResults = processVMWareExports(__dirname + "/testing/vm");
      wish(actualResults.length, expectedCount);
   });
});