const wish = require("wish");
const {readYamlFile, recursivelyProcessHelmDirectory} = require("./index");
const {isNil} = require("ramda");

describe("Basic Yaml Tests", () => {
    it("should read a YAML file", () => {
        const actual = readYamlFile("/Users/jason.guarracino/workspaces/qa3-k8s-apps/k8s/helm/adf/values.yaml");
        wish(!isNil(actual));
    });
});

describe("Complex Parsing Tests", () => {
   it("should process all values files", () => {
       const actual = recursivelyProcessHelmDirectory("/Users/jason.guarracino/workspaces/qa3-k8s-apps/k8s/helm");
       wish(!isNil(actual));
   });
});