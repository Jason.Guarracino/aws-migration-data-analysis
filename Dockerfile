# UNTESTED
FROM node
ADD src/ src/
ADD package.json package.json
ADD package-lock.json package-lock.json
RUN npm ci
USER nobody
ENTRYPOINT ["node -r src/index.js"]
