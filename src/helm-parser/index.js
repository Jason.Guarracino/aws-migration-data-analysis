const fs = require("fs");
const yaml = require("js-yaml");
const {equals, path, append, forEachObjIndexed, filter, map, reduce, includes, has, concat, join} = require("ramda");
const syspath = require("path");

const TEAM_NAMES = [
    "developers",
    "devops",
    "goal-diggers",
    "gobots",
    "hardcore-league",
    "havengers",
    "jollyllamas",
    "repeat-offenders",
    "sealteam6",
    "section-31",
    "security",
    "the-jedi-order",
    "thundercats",
    "unicorn-angi-force",
    "whiskey-business",
    "cmt-team",
];

const APP_TO_TEAM_OVERRIDES = [
    ["root-api", "havengers"],
    ["comm-render", "havengers"],
    ["root", "consumer-team"],
    ["pro", "havengers"],
    ["crm-file-server", "jollyllamas"],
    ["phonequeue", "jollyllamas"],
    ["sem-reporting", "jollyllamas"],
    ["comm-engine", "havengers"],
    ["spwebsite", "jollyllamas"],
    ["cmt", "cmt-team"],
];

const NO_TEAM_NAME = Symbol("NO_TEAM_NAME");

function lookupTeamName(appName){
    for(const [currentApp, currentTeam] of APP_TO_TEAM_OVERRIDES){
        if(equals(currentApp, appName)){
            return currentTeam;
        }
    }
    return NO_TEAM_NAME;
}

function recursivelyProcessHelmDirectory(directoryPath) {

    const items = fs.readdirSync(directoryPath);
    const isDirectory = (itemPath) => {
        const stats = fs.statSync(syspath.join(directoryPath, itemPath));
        return stats.isDirectory();
    };
    const directories = map(subDirectory => syspath.join(directoryPath, subDirectory), filter(isDirectory, items));
    const results = reduce((accum, value) => {
        const dirName = syspath.basename(value);
        let teamName = dirName;
        if (!includes(dirName, TEAM_NAMES)) teamName = "Unknown";

        const valuesFilePath = syspath.join(value, "values.yaml")
        if (fs.existsSync(valuesFilePath)) {
            const parsedFile = readYamlFile(valuesFilePath);
            const applications = processValuesFileKeys(teamName, parsedFile);
            return concat(applications, accum);
        }
        return accum;
    }, [], directories)
    return results;
}


function readYamlFile(path) {
    const result = yaml.load();
    return result;
}

function processValuesFileKeys(teamName, parsedValuesFile) {
    let results = [];
    const processEntry = (value, key) => {
        const app = parseApplication(teamName, key, value);
        if(!equals(NOT_INSTALLABLE, app)) results = append(app, results);
    }
    forEachObjIndexed(processEntry, parsedValuesFile);
    return results;
}

const NOT_INSTALLABLE = Symbol("Not Installable");

function parseApplication(teamName, applicationKey, applicationEntry) {
    let name = applicationKey;
    let workingTeamName = teamName;
    const getNameOverride = path(["nameOverride"]);
    const getServiceType = path(["service", "type"]);
    const getInstallCRDs = path(["installCRDs"]);
    if(has("installCRDs", applicationEntry) && !getInstallCRDs(applicationEntry)) return NOT_INSTALLABLE;
    if (!equals(applicationKey, getNameOverride(applicationEntry))) {
        name = getNameOverride(applicationEntry);
    }
    const serviceType = getServiceType(applicationEntry);
    const foundTeamName = lookupTeamName(name);
    if(!equals(foundTeamName, NO_TEAM_NAME)) workingTeamName = foundTeamName;
    if(name === undefined || serviceType === undefined) return NOT_INSTALLABLE;
    return {name, serviceType, team: workingTeamName};
}

function writeHelmDataToCSV(outputDir, applications) {
    let csvFile = [["Team Name", "Application Name"]];
    const appEntries = map(item => [item.team, item.name], applications);
    csvFile = concat(appEntries, csvFile);
    const lines = map(entry => join(",", entry), csvFile);
    const file = join("\n", lines);
    fs.writeFileSync(syspath.join(outputDir,"app-migration-list-generated.csv"), Buffer.from(file));
}

module.exports = {readYamlFile, parseApplication, processValuesFileKeys, recursivelyProcessHelmDirectory, writeHelmDataToCSV};